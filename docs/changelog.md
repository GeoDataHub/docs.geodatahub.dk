
A new version of GeoDataHub is currently released every 3 month. The version naming is `<year>.<zero-padded sequential number>`. So the fourth release in 2020 is version `2020.04`. This is released in end December 2020 at the end of Q4. Development releases are appended with the number of changes since the latest major release. As such, `2020.04.13` is the development version of `2020.04` with 13 code changed compared to the `2020.03` release. The development versioning is unique for each plugin/platform. The major versioning is synced same across all platforms (QGIS, Python, API, etc) to ensure comparability. Backward comparability is maintained for some time in the API until users have upgraded to the latest version.

## Changelog
### 2021.2

This release cycle has focused on making the project semi-ready to open source. There are still many areas that could use more documentation but I have decided to release, as is, and focus on the next iteration of the design. The code is now available on the [Gitlab project page](https://gitlab.com/GeoDataHub).

### 2021.1

This release cycle moved the main website from [geodatahub.dk](https://geodatahub.dk) to [getgeodata.com](https://getgeodata.com). The API endpoint (api.geodatahub.dk) remains the same for the foreseeable future.

The move was driven by a complete rewrite of the main business website that was long overdue for an update. The schema repository (https://schemas.getgeodata.com) also has a proper website now. This makes it a lot more accessible and helps users understand parameters.

The last general feature of this cycle was anonymous access. Previously, users had to log in before searching or accessing any dataset. This was an unnecessary barrier for many users looking for public data and not in line with the openness of the platform. Anonymous users now have read access to public datasets directly via the API. The web map is updated to also support anonymous browsing. QGIS and Python still need some changes to fully support this. Currently, there are no limits on the number of requests an anonymous user can make. This will change in the future.

#### API
* Allow anonymous users to get public datasets, search in public datasets, autocomplete public schemas, and get the data heatmap for all public data. All other endpoints/features still require a valid user.
* The API now returns a [HATEOAS](https://en.wikipedia.org/wiki/HATEOAS) style response when querying a single dataset (i.e. the `/datasets/:id` endpoint). The response encodes user permissions allowing client libraries to know which features are allowed (write, edit, publicize, share) for that dataset. This information is encoded in the `_links` section of the response.
* The API now exposes a new `contact` field and removed the raw `owner` and `organization` fields. This helps users quickly know who the main contact is for a given dataset.
* Support autocompleting schema parameters with spaces (e.g. `location name`).
* Requests can maximum return 15000 dataset. Otherwise, an informative error message is returned. In this case, the user should add more filters to the query.
* Search by geometry now returns all datasets that intersect the geometry. Previously only datasets that were entirely within the geometry were returned.

#### Web map
* The map no longer requires users to log in to support anonymous access.
* A new layer menu, in the upper right corner, allows users to hide layers.
* If the search returns datasets outside the current view users are guided to zoom out.
* The dataset view now allows users to quickly generate a direct link to a dataset making it easy to share with others.
* The `Links to data` makes it easier to see which files are available for a dataset.
* Additional buttons in the search drawer to clear and close the dialog.
* The search button is now a speed-dial to allow quick geometry search.
* Improve visualization of which search filters are active.
* Performance improvements.

#### Datasets
* Added [Florida Geological Survey (FGS) Wells](https://geodata.dep.state.fl.us/datasets/florida-geological-survey-fgs-wells?geometry=-97.108%2C24.815%2C-65.819%2C31.586)
* Added USGS [Airborne geophysical surveys](https://mrdata.usgs.gov/airborne) database for Florida

### 2020.4

This release cycle introduced the [user and permission management system](/Getting-started/users-and-permission/) that is a partial breaking change to the API. The map is updated and new QGIS and Python releases are expected in the start of January.

#### API
* Unique constraint on datasets - It is now possible to add `uniqueProperties` to the schema definition. Unique constraints ensures that only a single dataset within an organization can have the same parameters. This feature is great for datasets that are ingested from external databases. See [the docs](/Getting-started/schemas/#unique-constraints) or the [NPD wellbore schema](https://schemas.geodatahub.dk/organizations/npd/geophysics/well.json) for an example.

#### QGIS plugin
* Bugfix: To support failing login flow

### 2020.3

The main focus of this release cycle was the web frontend and backend stability.

#### Web frontend
* Complete rewrite of map.geodatahub.dk from scratch with support for latest API changes

#### API
* Support for nested schemas

#### Other
* New `unique` parameter to schemas
* New `well` schema from NPD

### 2020.2

The main focus of this release cycle was usability of the QGIS plugin with support for the new search panel.

#### QGIS plugin
* Add new search panel
* Allow users to search by schema parameters with auto-complete on options
* Allow users to search by datatype
* Allow users to search by geography area

#### Python library
* Add support for Microsoft Windows

#### API
* Support Geographic search with user-defined polygons
* Support search by datatype (i.e. all dataset with a specific schema)
* Add `/schemas/autocomplete` endpoint to support auto-complete in plugins

#### Other
* Add `datatype` parameter to all schemas
* Complete rewrite of docs.geodatahub.dk
* Support for HTTPS on schemas.geodatahub.dk
* Add more showcase dataset
* And countless bug fixes and improvements
