# Authentication

!!! tip
    Looking for our [API Reference](https://docs.geodatahub.dk/api-specs.html)? Or want to query the API directly from [Python](Python-library/getting-started/).

GeoDataHub uses the secure [Oauth 2.0 protocol](https://aaronparecki.com/oauth-2-simplified/) to authenticate users. All users are provided a time limited access token that uniquely identifies them on the platform.

Once authenticated users receive three tokens,

* `Access Token`: Used to inform the backend about the users' access level
* `Refresh Token`: Used to obtain a new ID and access tokens
* `Identity Token`: Used to identify the user

!!! important
    The access and identity tokens expires after 1 hour. Refresh tokens expire after 27 days.

Each API call must contain a valid **identity token** that informs the backend of who is requesting access. Tokens, therefore, standing for users against the API. If a token is lost or compromised logout from the platform and obtain fresh tokens. It is important to store tokens securely and not transfer tokens across insecure channels.

## Login via Python

The fastest way for developers to obtain a valid token is using the command line tool,

```bash
gdh login --print-all-tokens
```

If you are already logged in use the `--use-refresh-token` option to obtain new tokens. 

The login command automatically stores the token in the users home folder. On Linux/Mac `/home/<USERNAME>/.gdh/authentication` or on Windows `c:\Users\<CurrentUserName>\.gdh\authentication`. The `authentication` file is JSON encoded and contains a single key.

## Using the web-based login

Users can login or register via the browser using [geodatahub.dk/login](www.geodatahub.dk/login) using the default *Authorization Code flow*. After logging in the user is given a one-time code they may exchange for a valid *access token*.  Submit the code to obtain the tokens,

=== "HTTP"
    ``` bash
    POST https://auth-dev.geodatahub.dk/oauth/token
    
    HEADERS
      "Content-type": "application/x-www-form-urlencoded"
      
    BODY
      grant_type: 'authorization_code'
      client_id: '7aetu7cj45ah6m494hutdldbd1'
      code: <THE ONE-TIME CODE>
      redirection_uri: 'https://www.geodatahub.dk/validate-code.html'
    ```

=== "Python"
    ``` python
    import requests
    
    payload = {
        'grant_type':'authorization_code',
        'client_id':'7aetu7cj45ah6m494hutdldbd1',
        'code':code,
        'redirect_uri': 'https://www.geodatahub.dk/validate-code.html'
    }
    headers = {'Content-type': 'application/x-www-form-urlencoded'}

    auth_url = "https://auth-dev.geodatahub.dk/oauth/token"
    resp = requests.post(auth_url, data=payload, headers=headers)
    print(resp.json())
    ```


A successful authorization returns the response,

```json
HTTP/1.1 200 OK
Content-Type: application/json

{ 
 "access_token":"eyJz9sdfsdfsdfsd", 
 "refresh_token":"dn43ud8uj32nk2je", 
 "id_token":"dmcxd329ujdmkemkd349r",
 "token_type":"Bearer", 
 "expires_in":3600
}
```

## Sending authorized requests

Authenticate against the API by including the access token in the `Authorization` header parameter.

=== "HTTP"
    ``` bash
    GET https://api-dev.geodatahub.dk/datasets
    
    HEADERS
      "Authorization": "NgCXRK...MzYjw"

    ```

=== "Command line"
    ``` bash
    curl -X GET -H 'Authorization: eyJ...r9A' https://api-dev.geodatahub.dk/datasets
    ```

=== "Python"
    ``` python
    import requests
    
    url = "https://api-dev.geodatahub.dk/datasets"
    headers = {"Authorization": "Basic ZjM4Zj...Y0MzE="}
    
    resp = requests.get(url, headers=headers)
    print(resp.json())
    ```

If successful the command will return a JSON encoded list of all public datasets.

Learn more about the Web API in the [basic operations](/Web-API/basic-operations) guide.

## Reference
* [API Reference](https://docs.geodatahub.dk/api.html)
