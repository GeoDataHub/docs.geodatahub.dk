# Basic API operations

The GeoDataHub Web API is based on [REST principles](https://restfulapi.net/). Readers not familiar with REST APIs are encouraged to understand [the basics](https://gist.github.com/alexserver/2fcc26f7e1ebcfc9f6d8) before reading on.

Data resources are accessed via standard HTTP requests. Where possible, the web API uses appropriate HTTP verbs for each action.

| Method | Action                            |
|--------|-----------------------------------|
| GET    | Retrieves resources               |
| POST   | Creates resources                 |
| PUT    | Changes and/or replaces resources |
| DELETE | Deletes resources                 |

The API is split into operations on different resources. Operations specific to datasets use the URI `https://api-dev.geodatahub.dk/datasets/`. Depending on the HTTP method the same URI can get, add, change, or remove datasets. See the **examples** section for more.

## Global data model

Each dataset contains a fixed set of metadata attributes defined by the platform and a variable set of metadata defined by the [schema](/Getting-started/schemas/). The schema specific parameters are stored under the `distribution_info` key while all other parameters exists for all datasets,

```json
{
  "identifier": "6376f381-09f0-4dbe-a994-a88a4847defe",
  "owner": "aa59467e-9aef-4854-a762-920fe20a9747",
  "organization": "00000000-0000-0000-0000-000000000000",
  "datatype": "https://schemas.geodatahub.dk/organizations/geus/geophysics/wenner.json",
  "citation": "",
  "description": "Odense Syd, historiske Wenner data",
  "larger_work": "",
  "distribution_info": {
    "Ident": "dk.mim.ode.wenner-odensesyd.Lindved_WEN_3",
    "Project": "Odense Syd, historiske Wenner data",
    "Purpose": "saarbarhed",
    "Contractor": "COWI A/S",
    "Dataset Id": 700849,
    "Project Id": 1508,
    "Maximum RHoa": 57.0,
    "Minimum RHoa": 46.0,
    "Inserted Date": "2009-01-09",
    "Project Ident": "dk.mim.ode.wenner-odensesyd",
    "Maximum A-distance": 25.0,
    "Minimum A-distance": 25.0,
    "Maximum Focus Depth": 12.976,
    "Minimum Focus Depth": 12.976,
    "Number Of Positions": 24
  },
  "result": {
    "download_link": "http://gerda.geus.dk/Gerda/dataset/show/700849"
  },
  "projected_geometry": {
    "type": "LineString",
    "coordinates": [
      [
        10.398255,
        55.329524
      ],
      [
        10.404169,
        55.33369
      ]
    ]
  }
}
```

The platform specific attributes are defined in the [API reference](https://docs.geodatahub.dk/api-specs.html#tag/datasets/paths/~1datasets/post). For convenience the most important are listed below,

| Parameter            | Definition                                                                                                                                                        |
|----------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `identifier`         | Random unique identifier following the [uuid v4](https://en.wikipedia.org/wiki/Universally_unique_identifier) format (ex. `6376f381-09f0-4dbe-a994-a88a4847defe`) |
| `result`             | JSON encoded list with information linking the metadata information with one or more files                                                                        |
| `projected_geometry` | The 2D geometry, in GeoJSON format, of the dataset (`POINT`, `LINE`, or `POLYGON`)                                                                                |
| `datatype`           | The full schema URI that defines the `distribution_info` section                                                                                                  |
| `distribution_info`  | Dataset specific metadata attributes                                                                                                                              |

The variable names are based on the [INSPIRE Geoph Measurement](https://inspire-regadmin.jrc.ec.europa.eu/dataspecification/ScopeObjectDetail.action?objectDetailId=10456) class.

### The result section

!!! note
    The result document and backend logic is still under development and is not considered stable.

Metadata is linked to the actual files or data streams containing the data through the `result` parameter. The file information is encoded as JSON allowing flexibility in the possible parameters to include.
The `result` parameter must include the following attributes,

- **uri**: Local or remote path to the file or data stream
- **protocol**: The protocol used to access the data (e.g. `HTTP`, `FTP`, `FILE`, etc)
- **file extension**: The file extension (e.g. `pdf`, `las` etc.)
- **source**: The software or instrument used to generate, process or transform the data

Possible additional attributes are,

- **md5**: A md5 hash sum of the file content
- **license**: URL to the terms of use for public or commercial data

Example of the `result` parameter,
```json
[{
  "uri": "https://txdata.usgs.gov/GeoLogArchiver/odata/Logs(1618)/LogFile",
  "location": "HTTPS",
  "file extension": "las",
  "source": "unknown",
  "md5": "060b49d83ba4ea60aefcde8885bad26c"
}]
```

## Example requests

| Description                                                                                                              | HTTP Method | Request format                                                                                                                                       |
|--------------------------------------------------------------------------------------------------------------------------|-------------|------------------------------------------------------------------------------------------------------------------------------------------------------|
| Get a specific Dataset with ID `6376f381-09f0-4dbe-a994-a88a4847defe`                                                    | GET         | https://api-dev.geodatahub.dk/datasets/6376f381-09f0-4dbe-a994-a88a4847defe                                                                          |
| Add a new dataset (See [required payload](https://docs.geodatahub.dk/api-specs.html#tag/datasets/paths/~1datasets/post)) | POST        | https://api-dev.geodatahub.dk/datasets                                                                                                               |
| Get all datasets with schema `https://schemas.geodatahub.dk/organizations/usgs/geophysics/geolog.json`                   | GET         | `https://api-dev.geodatahub.dk/datasets?q={"https://schemas.geodatahub.dk/organizations/usgs/geophysics/geolog.json": ""}`                           |
| Get all Geolog datasets starting below 10 ft                                                                             | GET         | `https://api-dev.geodatahub.dk/datasets?q={"https://schemas.geodatahub.dk/organizations/usgs/geophysics/geolog.json":{"DepthStartFt": {"$gt": 10}}}` |
| Get all data within a polygon                                                                                            | GET         | `https://api-dev.geodatahub.dk/datasets?q={"geometry":{\"$contains\":\"POLYGON((7.9 55.4,8.1 55.4,8.1 55.6,7.9 55.6,7.9 55.4))\"}}`                  |

## Search format

GeoDataHub supports three modes of search,

- **Metadata search**: Find all datasets with a specific metadata attribute
- **Datatype search**: Find all datasets of a specific datatype
- **Geography search**: Find all datasets within a geographic area

Users can freely combine different modes depending on their needs.

A dataset search encodes the query as a JSON formatted parameter in the URL. All search requests are sent to the `https://api-dev.geodatahub.dk/datasets` URL with the query encoded as `q` (e.g. `https://api-dev.geodatahub.dk/datasets?q="YOUR QUERY"`). 

The search either contains a `schema` or `geometry` at the top-level. To search for all Geolog data send the following query,

```JSON
{
"https://schemas.geodatahub.dk/organizations/usgs/geophysics/geolog.json": ""
}
```

The URL for this query is,

```bash
GET https://api-dev.geodatahub.dk/datasets?q={"https://schemas.geodatahub.dk/organizations/usgs/geophysics/geolog.json": ""}

HEADERS
  "Authorization": "NgCXRK...MzYjw"

```

The empty value indicates no filtering is needed beyond the schema. A search for all Geolog data within Florida is,

```JSON
{
"https://schemas.geodatahub.dk/organizations/usgs/geophysics/geolog.json":
    {
	"StateName": "Florida"
    }
}
```

`StateName` is defined in the [Geolog schema](https://schemas.geodatahub.dk/organizations/usgs/geophysics/geolog.json).

Currently, the query language supports two simple logical operators (`$lt` less than and `$gt` greater than). To filter to only logs in Florida that are also deeper than 90 ft,

```JSON
{
"https://schemas.geodatahub.dk/organizations/usgs/geophysics/geolog.json": {
    "StateName": "Florida",
    "DepthEndFt": {"$gt": 90}
}
}
```

Query parameters are additive so **both** conditions are required on the dataset. The logical `OR` operation (i.e. one or the other) is currently not supported.

Searching within a geographic region is supported using the top-level `geometry` parameters. The geometry is defined by a polygon encoded as [Well known text](https://en.wikipedia.org/wiki/Well-known_text_representation_of_geometry).

```JSON
{
"geometry": {
    "$contains": "POLYGON((7.9 55.4,8.1 55.4,8.1 55.6,7.9 55.6,7.9 55.4))",
	}
}
```

Since no additional constraints are imposed the query above returns all datasets within the polygon.

Finally, let's combine two search modes in a single query,

```JSON
{
"https://schemas.geodatahub.dk/organizations/usgs/geophysics/geolog.json": {
    "Category": "Electromagnetic"
},
"geometry": {
    "$contains": "POLYGON ((-70.39215087890625 41.16211393939692, -69.81674194335938 41.16211393939692, -69.81674194335938 41.447873389865194, -70.39215087890625 41.447873389865194, -70.39215087890625 41.16211393939692))",
    }
}
```

This will return all Geolog datasets of the category `Electromagnetic` located on the island of Nantucket off the east coast of the US.
