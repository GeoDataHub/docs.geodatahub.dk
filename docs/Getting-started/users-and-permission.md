# Users, organizations and permissions

GeoDataHub supports a [Role-based access control](https://en.wikipedia.org/wiki/Role-based_access_control) (RBAC) model to define permissions.  RBAC contains the following concepts,

- **Subject** The user making the request
- **Object** The dataset, organization, or similar resource in question
- **Operation** The type of request `create a new dataset`, `list all users in an organization`, etc.

    In comparison to other permission models (e.g. access control lists) users are not granted permission directly. Users get their permissions by assuming a role that has specific access to a resource. A role might hold the `read` and `share` permissions within an organization. Users who can assume that role is allowed to `read` and `share` any dataset within the organization. Roles only apply to a single organization and its child organizations. It is not possible to transfer roles between organizations. If users need the same permissions across multiple organizations use nested organizations ([Hierarchical organizations](#hierarchical-organizations-and-role-inheritance)).

## Sharing levels
GeoDataHub supports four levels for sharing datasets,

| Level | Description |
|-------|-------------|
| Private | The dataset is only accessible by the owner |
| Organizational | The dataset is shared with all members of an organization |
| Shared | The dataset is shared within an organization and one or more users outside the organization can `view` the dataset |
| Public | The dataset is searchable by anyone |

### Private dataset
A private dataset belongs to a single user (the owner). No one, except for the user, can search, view, modify, or delete the dataset. This is the most restrictive sharing mode. This most is mostly used for testing or in very small organizations.

### Organizational dataset
In this mode, the dataset is shared with all members of an organization or parent organizations ([See hierarchical organizations](#hierarchical-organizations-and-role-inheritance)). Organizational datasets support permission levels allowing different access dependent on the users' roles. E.g. some users can only read the data while other users can modify or share the data.

This is the most common sharing mode.

### Sharing dataset with externals
Sometimes, you may wish to share data, as read-only, with users who are not part of your organization (e.g. external collaborations or contractors). Using the **sharing** functionality you can invite any GeoDataHub user to view a single dataset.

If the user needs access to many dataset or requires `write` permission, create a child organization and move the datasets. That way, you can customize access to fit your needs.

### Making dataset public outside the organization
Publicizing a dataset is similar to sharing, except it enables public read-only access. In this mode, any GeoDataHub user is able to search and view the dataset.

The public sharing level applies directly to a single dataset. This allows organization administrators to share single datasets within an organization while keeping other close to the public.

## Permission levels
As previously mentioned, permissions grant roles access to perform operations on datasets, users, or organizations. GeoDataHub implements several permissions to customize access,

| permission | description |
|------------|-------------|
| read  | The permission allows a role to read datasets (always allowed) |
| write | The permission allows a role to add new, modify existing, or delete datasets |
| share | The permission allows a role to share datasets with users outside the organization |
| Manage users | The permission allows a role to add or remove users from the organization |
| Manage organization | The permission allows a role to change settings about the organization in general (e.g. the organization name, add child organizations, etc) |

Mixing these permissions allows organization administrators to create custom roles that match their organization. Common combinations are,

- **Read-only user** - Read
- **Contributor** - Read, Write, Share
- **Superuser** - All permissions

## Hierarchical organizations and role inheritance
Most cor have a hierarchical or nested structure (e.g. when multiple departments are part of the same corporate division). In this case, it makes sense to structure your organizations to match your corporate structure.

![hierarachical organization example](../assets/img/org-diagram.png)

In the above diagram, departments are nested. Using hierarchical organizations allows roles to inherit permissions from parent to all child organizations. This makes managing users and roles a lot easier.

A user with `read` permission in the `fieldwork` division can view any data within `drilling` and `geophysics` but cannot access anything else. The `read` permission is inherited from the parent (`fieldwork`) to all children (`drilling` and `geophysics`). If the user requires `write` access to `drilling` that is possible by adding the user to a role that holds the `write` permission in that organization. Inheritance only applies from parent to child. The user will not have `write` permission in `geophysics` since the organizations are siblings.

The `data management` is located at the top since they should have access to all data.
