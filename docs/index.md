# Welcome to GeoDataHub

**This project is an experiment to build a geoscientific data catalog using a modern cloud architecture and focus on collecting all data types within a single system. The project is stable but not production-ready and no longer maintained in it's current form. It's released to inspire others and showcase how to build such a platform.**

GeoDataHub is an open-source geoscientific data catalog. It allows users to link metadata stored in a database with data stored elsewhere (e.g. on a shared file drive or in cloud storage). By using flexible schemas it's easy to store multiple data types within the same database to break down data silos. It also supports storing the same data type using multiple different metadata schemas (e.g. it's possible to show well information from both the Norwegian and Danish geological survey on the same map). Metadata is described using the JSON schema format which allows data managers to enforce validation rules. This ensures high-quality metadata. This feature supports building a single unified data catalog that links directly to the relevant data.

## Getting started

GeoDataHub is designed to integrate directly with your faviorite way of working. To get started with GeoDataHub, choose your desired platform,

* [Getting started with our QGIS plugin](QGIS-plugin/getting-started/)
* [Getting started with our Python package](Python-library/getting-started/)
* [Getting started with the command line tools](cli/getting-started/)
* [Getting started with the Web API](Web-API/getting-started/)
