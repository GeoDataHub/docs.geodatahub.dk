Documentation for the GeoDataHub project.

# License

The content of this repository is distributed under a [Creative Commons Share-Alike license](https://creativecommons.org/licenses/by-sa/4.0/).
